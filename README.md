# dmenu - dynamic menu
This is my build of the [suckless dynamic menu (dmenu)](https://tools.suckless.org/dmenu/) based on 1a13d0465d (v5.0) with a couple of extra features.

## Installation
```
git clone https://gitlab.com/jiri.hajek/dmenu.git
cd st
sudo make install
```

## Patches
* dmenu-caseinsensitive-5.0
* dmenu-center-20200111-8cd37e1
* dmenu-grid-4.9
* dmenu-gridnav-5.0
* dmenu-highlight-4.9
* dmenu-lineheight-5.0

## Additional features
* added `Run:` prompt to `dmenu_run`
* default line height is 24 pixels
* emoji support

## Fonts
This build uses the `Hack Nerd Font Mono` and `JoyPixels` fonts.

## Emoji support
You'll need the `JoyPixels` font to display emojis.

If you have trouble viewing emojis, install [libxft-bgra](https://aur.archlinux.org/packages/libxft-bgra) and/or [lib32-libxft-bgra](https://aur.archlinux.org/packages/lib32-libxft-bgra) from the AUR. You'll most likely have to do this unless [this MR](https://gitlab.freedesktop.org/xorg/lib/libxft/-/merge_requests/1) has already been merged.

![emojis](./screenshots/emojis.png)
